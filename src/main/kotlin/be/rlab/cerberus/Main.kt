package be.rlab.cerberus

import be.rlab.cerberus.config.ApplicationBeans
import be.rlab.tehanu.config.BotBeans
import be.rlab.tehanu.config.DataSourceBeans
import be.rlab.tehanu.config.MemoryBeans
import be.rlab.tehanu.domain.Tehanu
import be.rlab.tehanu.util.persistence.DataSourceInitializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import org.springframework.beans.factory.getBean
import org.springframework.context.annotation.AnnotationConfigApplicationContext

class Main {

    private val logger: Logger = LoggerFactory.getLogger(Main::class.java)

    fun start() {
        logger.info("Antifaz is waking up")

        val context = AnnotationConfigApplicationContext {
            DataSourceBeans.beans().initialize(this)
            MemoryBeans.beans().initialize(this)
            BotBeans.beans().initialize(this)
            ApplicationBeans.beans().initialize(this)
            refresh()
        }

        logger.info("Initializing data source")
        val dataSourceInitializer: DataSourceInitializer = context.getBean()
        dataSourceInitializer.initAuto()

        val tehanu: Tehanu = context.getBean()
        logger.info("Cerberus is entering Telegram")
        tehanu.start()
    }
}

fun main(args: Array<String>) {
    // Sets up jul-to-slf4j bridge. I have not idea
    // why the logging.properties strategy doesn't work.
    SLF4JBridgeHandler.removeHandlersForRootLogger()
    SLF4JBridgeHandler.install()

    Main().start()
}
