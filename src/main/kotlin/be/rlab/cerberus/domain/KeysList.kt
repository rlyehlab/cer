package be.rlab.cerberus.domain

import be.rlab.cerberus.domain.MemorySlots.KEYS
import be.rlab.cerberus.domain.model.Key
import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.Message
import be.rlab.tehanu.domain.model.TextMessage

/** Command to display the list of keys.
 */
class KeysList(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val accessControl: AccessControl,
    memory: Memory
) : Command {

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: Message
    ): MessageContext {
        require(message is TextMessage)
        return if (keys.isEmpty()) {
            context.answer("no tengo llaves registradas")
        } else {
            val chatKeys: List<Key> = keys.filter { key ->
                key.chatId == context.chat.id
            }
            val keysList: String = chatKeys.joinToString("\n") { key ->
                val holderName: String = key.holderId?.let {
                    accessControl.findUserById(key.holderId)?.userName
                } ?: key.holderName ?: "unresolved"

                "${key.name}: $holderName"
            }

            context.talk("# Juegos de llaves\n\nTotal: ${chatKeys.size} llave(s)\n\n$keysList")
        }
    }
}
