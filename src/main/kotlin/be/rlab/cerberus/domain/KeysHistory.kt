package be.rlab.cerberus.domain

import be.rlab.cerberus.domain.MemorySlots.KEYS
import be.rlab.cerberus.domain.model.Key
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.Message
import be.rlab.tehanu.domain.model.TextMessage

/** Command to display the history of a key.
 */
class KeysHistory(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    memory: Memory
) : Command {

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: Message
    ): MessageContext {
        require(message is TextMessage)
        val keyId: String = if (context.retries == 0) {
            message.text.substringAfter(" ").trim()
        } else {
            message.text.trim()
        }

        return if (keyId.isEmpty()) {
            context.talk(
                "decime el nombre de la llave que querés ver el historial, " +
                "podés ver el nombre de las llaves con el comando /keys"
            )
        } else {
            val chatKeys: List<Key> = keys.filter { key ->
                key.chatId == context.chat.id
            }
            val key: Key = context.require(
                chatKeys.find { key ->
                    key.name == keyId
                },
                "la llave $keyId no está registrada, registrala con /register_keys"
            )

            val history: String = key.history.joinToString("\n") { event ->
                "[${event.date.toString("YYYY-MM-dd")}] [${event.userName}]: ${event.description}"
            }

            context.talk(history)
        }
    }
}
