package be.rlab.cerberus.domain.model

import org.joda.time.DateTime

/** Represents a single event in a [Key] history.
 */
data class Event(
    /** Date this entry was created. */
    val date: DateTime,
    /** User that triggered this event. */
    val userName: String,
    /** Event description. */
    val description: String
) {
    companion object {
        fun new(
            userName: String,
            description: String
        ) = Event(
            date = DateTime.now(),
            userName = userName,
            description = description
        )
    }
}
