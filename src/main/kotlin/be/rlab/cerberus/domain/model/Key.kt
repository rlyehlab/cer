package be.rlab.cerberus.domain.model

import java.util.*

/** Represents a single key.
 *
 * Keys have identifiers so it is easier to know who has which key.
 * They also have a history to understand what happened with a key over the time.
 */
data class Key(
    /** The unique key id. */
    val id: UUID,
    /** Chat id this key belongs to. */
    val chatId: UUID?,
    /** A human name for the key. */
    val name: String,
    @Deprecated("for backward-compatibility only, use holderId instead")
    val holder: Long,
    /** Id of the user responsible of this key. */
    val holderId: Long? = holder,
    /** Name of the user responsible of this key. */
    val holderName: String?,
    /** Key history. */
    val history: List<Event>
) {
    companion object {
        fun new(
            chatId: UUID,
            name: String,
            holderId: Long?,
            holderName: String
        ): Key = Key(
            id = UUID.randomUUID(),
            chatId = chatId,
            name = name,
            holder = holderId ?: 0,
            holderId = holderId,
            holderName = holderName,
            history = listOf(Event.new(holderName, "Se registró llave"))
        )
    }

    /** Transfers this key to another user.
     * @param newHolderId New holder identifier.
     * @param newHolderName New holder name.
     * @param cause Event that caused this key to be transferred.
     */
    fun transfer(
        newHolderId: Long?,
        newHolderName: String,
        cause: Event
    ): Key = copy(
        holderId = newHolderId,
        holder = newHolderId ?: 0,
        holderName = newHolderName,
        history = history + cause
    )

    /** Renames this key.
     * @param newName New key name.
     * @param event Event that caused this key to be transferred.
     */
    fun rename(
        newName: String,
        event: Event
    ): Key = copy(
        name = newName,
        history = history + event
    )
}
