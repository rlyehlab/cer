package be.rlab.cerberus.domain

import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import be.rlab.tehanu.domain.model.User
import be.rlab.cerberus.domain.MemorySlots.KEYS
import be.rlab.cerberus.domain.model.Key
import be.rlab.tehanu.domain.model.Message

/** Command to register new keys.
 */
class RegisterKeys(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val accessControl: AccessControl,
    memory: Memory
) : Command {

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: Message
    ): MessageContext = context.userInput("Pasame la información de las nuevas llaves con el siguiente formato:") {
        val name: String by field("un nombre para reconocer este juego de llaves")
        val holder: String by field("nombre de usuario de telegram que tiene las llaves (opcional, si no se especifica es la persona que usa el comando)")

        onSubmit {
            val exists: Boolean = keys.any { key ->
                key.name == name
            }

            if (exists) {
                answer("ya existe una llave con el id ${name}")
            } else {
                val holderName: String = holder.trim().substringAfter("@")
                val user: User? = accessControl.findUserByName(holderName)

                val newKey: Key = Key.new(
                    chatId = context.chat.id,
                    name = name,
                    holderId = user?.id,
                    holderName = holderName
                )

                keys = keys + newKey
                answer("listo, la nueva llave la tiene @$holderName")
            }
        }
    }
}
