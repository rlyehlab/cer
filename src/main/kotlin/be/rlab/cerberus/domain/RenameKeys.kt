package be.rlab.cerberus.domain

import be.rlab.cerberus.domain.MemorySlots.KEYS
import be.rlab.cerberus.domain.model.Event
import be.rlab.cerberus.domain.model.Key
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.Message
import be.rlab.tehanu.domain.model.TextMessage

/** Command to rename a key.
 */
class RenameKeys(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    memory: Memory
) : Command {

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: Message
    ): MessageContext = context.userInput("Decime qué llaves querés renombrar y el nuevo nombre:"){
        val keyName: String by field("nombre de la llave")
        val newName: String by field("nuevo nombre de la llave")

        onSubmit {
            val chatKeys: List<Key> = keys.filter { key ->
                key.chatId == context.chat.id
            }
            val key: Key = require(
                chatKeys.find { key ->
                    key.name == keyName
                },
                "la llave ${keyName} no está registrada, registrala con /register_keys"
            )
            val transferredKey: Key = key.rename(
                newName = newName,
                event = Event.new(
                    userName = user?.userName ?: user!!.id.toString(),
                    description = "Se renombró la llave de '${key.name}' a '${newName}'"
                )
            )

            keys = keys.map { existingKey ->
                if (existingKey.name == key.name) {
                    transferredKey
                } else {
                    existingKey
                }
            }

            answer("listo, se renombró la llave")
        }
    }
}
