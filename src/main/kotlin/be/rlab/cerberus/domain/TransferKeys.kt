package be.rlab.cerberus.domain

import be.rlab.cerberus.domain.MemorySlots.KEYS
import be.rlab.cerberus.domain.model.Event
import be.rlab.cerberus.domain.model.Key
import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.Message
import be.rlab.tehanu.domain.model.TextMessage
import be.rlab.tehanu.domain.model.User

/** Command to transfer a key from a user to another user.
 */
class TransferKeys(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val accessControl: AccessControl,
    memory: Memory
) : Command {

    data class KeyHolderInput(
        val key: String,
        val holder: String,
        val cause: String?
    )

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: Message
    ): MessageContext = context.userInput("Decime qué llaves querés transferir con el siguiente formato:") {
        val keyName: String by field("nombre de la llave")
        val holder: String by field("nombre de usuario de telegram a quien le transferís la llave")
        val causeMessage: String by field("por qué se está transfiriendo la llave (opcional)")

        onSubmit {
            val holderName: String = holder.substringAfter("@").trim()
            val newHolder: User? = accessControl.findUserByName(holderName)
            val chatKeys: List<Key> = keys.filter { key ->
                key.chatId == context.chat.id
            }
            val key: Key = require(
                chatKeys.find { key ->
                    key.name == keyName
                },
                "la llave ${keyName} no está registrada, registrala con /register_keys"
            )
            val cause = "Se transfirió la llave ${key.name} a $holderName"
            val transferredKey: Key = key.transfer(
                newHolderId = newHolder?.id,
                newHolderName = holderName,
                cause = Event.new(
                    userName = user?.userName ?: user!!.id.toString(),
                    description = causeMessage.let {
                        "$cause: ${causeMessage}"
                    } ?: cause
                )
            )

            keys = keys.map { existingKey ->
                if (existingKey.name == key.name) {
                    transferredKey
                } else {
                    existingKey
                }
            }

            answer("listo, ahora la llave la tiene @${holder}")
        }
    }
}
