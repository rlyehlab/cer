package be.rlab.cerberus.domain

import be.rlab.cerberus.domain.MemorySlots.KEYS
import be.rlab.cerberus.domain.model.Key
import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.Message
import be.rlab.tehanu.domain.model.TextMessage
import java.util.*

/** Command to migrate and fix data when new versions are not backward-compatible
 * with previous versions.
 */
class Fixups(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val accessControl: AccessControl,
    memory: Memory
) : Command {

    private var keys: List<Key> by memory.slot(KEYS, listOf<Key>())

    override fun handle(
        context: MessageContext,
        message: Message
    ): MessageContext {
        require(message is TextMessage)
        return if (message.text.startsWith("/")) {
            val chatList: String = accessControl.listChats().joinToString("\n") { chat ->
                "${chat.id} - ${chat.title}"
            }
            //context.talk(chatList).retry()
            context
        } else {
            val uuid: UUID = UUID.fromString(message.text.trim())
            keys = keys.map { existingKey ->
                val chatId: UUID = existingKey.chatId ?: uuid
                existingKey.copy(
                    chatId = chatId
                )
            }
            //context.talk("listo").done()
            context
        }
    }
}
