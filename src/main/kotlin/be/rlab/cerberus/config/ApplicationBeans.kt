package be.rlab.cerberus.config

import be.rlab.cerberus.domain.*
import be.rlab.tehanu.domain.model.ChatType
import org.springframework.context.support.beans

object ApplicationBeans {
    fun beans() = beans {
        // Listeners
        bean {
            KeysHistory(
                name = "/keys_history",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = -1,
                memory = ref()
            )
        }
        bean {
            KeysList(
                name = "/keys",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = -1,
                accessControl = ref(),
                memory = ref()
            )
        }
        bean {
            RegisterKeys(
                name = "/register_keys",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = 120000,
                accessControl = ref(),
                memory = ref()
            )
        }
        bean {
            TransferKeys(
                name = "/transfer_keys",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = 120000,
                accessControl = ref(),
                memory = ref()
            )
        }
        bean {
            RenameKeys(
                name = "/rename_keys",
                scope = listOf(ChatType.GROUP, ChatType.PRIVATE),
                timeout = 120000,
                memory = ref()
            )
        }
        bean {
            Fixups(
                name = "/fixups",
                scope = listOf(ChatType.PRIVATE),
                timeout = 120000,
                accessControl = ref(),
                memory = ref()
            )
        }
    }
}
