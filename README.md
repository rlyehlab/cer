# Cerberus

Cerberus es un bot de Telegram que maneja llaveros. Cada chat donde se agregue Cerberus va a tener su propio llavero.

Cada juego de llaves tiene un nombre para identificarlo y una persona responsable.

## Comandos

*/register_keys*: registra un nuevo juego de llaves. Uso: /register_keys

*/keys*: muestra todos los juegos de llaves y quiénes los tienen. Uso: /keys

*/transfer_keys*: transfiere un juego de llaves a otra persona. Uso: /transfer_keys

*/keys_history*: muestra el historial de eventos de un juego de llaves. Uso: /keys_history [nombre_de_llaves]

*/rename_keys*: renombra un juego de llaves. Uso: /rename_keys
